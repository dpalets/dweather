//
//  FcastTableViewCell.swift
//  DWeather
//
//  Created by Dmytro Palets on 11/19/18.
//  Copyright © 2018 Dmytro Palets. All rights reserved.
//

import UIKit

class FcastTableViewCell: UITableViewCell {
    
    @IBOutlet weak var fcastTimeLabel: UILabel!
    @IBOutlet weak var fcastCondView: UIImageView!
    @IBOutlet weak var fcastTempLabel: UILabel!
    @IBOutlet weak var fcastWindLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

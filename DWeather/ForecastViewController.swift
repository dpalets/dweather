//
//  ForecastViewController.swift
//  DWeather
//
//  Created by Dmytro Palets on 11/5/18.
//  Copyright © 2018 Dmytro Palets. All rights reserved.
//

import UIKit

class ForecastViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var placeName : String?
    var countryName : String?
    
    var forecastData : ViewController.ForecastData?
    var fcastTableData : [String : [ViewController.ForecastTableStuff]]?
    
    var sections_raw = [String]()
    var sections = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        if countryName! != "" {
            titleLabel.text = "Detailed forecast: " + placeName! + ", " + countryName!
        } else
            {titleLabel.text = "Detailed forecast: " + placeName!}
        
      sections_raw = fcastTableData!.keys.sorted()
        for i in 0..<sections_raw.count {
            sections.append(String(sections_raw[i].dropFirst()))
        }

    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return sections[section]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        let sections_raw = fcastTableData!.keys.sorted()
        return sections_raw.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return fcastTableData![sections_raw[section]]!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "fcastCell", for: indexPath) as! FcastTableViewCell
        
        cell.fcastTimeLabel.text = fcastTableData![sections_raw[indexPath.section]]![indexPath.row].time

        cell.fcastCondView.image = getIcon("https://openweathermap.org/img/w/\(fcastTableData![sections_raw[indexPath.section]]![indexPath.row].cond).png")
        
        cell.fcastTempLabel.text = String(fcastTableData![sections_raw[indexPath.section]]![indexPath.row].temp) + "\u{00B0}"

        cell.fcastWindLabel.text = fcastTableData![sections_raw[indexPath.section]]![indexPath.row].wind
  
        return cell
    }
    
    func getIcon(_ iconURLString: String) -> UIImage {
        
        let iconUrl : URL = URL(string: iconURLString)!
        let iconData:NSData = NSData(contentsOf: iconUrl)!
        let icon = UIImage(data: iconData as Data)
        return icon!
    }

}

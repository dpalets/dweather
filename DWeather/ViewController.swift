//
//  ViewController.swift
//  DWeather
//
//  Created by Dmytro Palets on 10/20/18.
//  Copyright © 2018 Dmytro Palets. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Foundation

class ViewController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {
    
    

    @IBOutlet weak var placeLookupField: UITextField!
    @IBOutlet weak var placeSearchButton: UIButton!
    @IBOutlet weak var currentPosButton: UIButton!
    
    @IBOutlet weak var currentWeatherIconView: UIImageView!
    @IBOutlet weak var currCondTitle: UILabel!
    @IBOutlet weak var currCondPlaceLabel: UILabel!
    @IBOutlet weak var currCondTempLabel: UILabel!
    @IBOutlet weak var currCondDescLabel: UILabel!
    @IBOutlet weak var currCondWindLabel: UILabel!
    @IBOutlet weak var currCondHumLabel: UILabel!
    
    
    @IBOutlet weak var fcastDateLabel1: UILabel!
    @IBOutlet weak var fcastIconView1: UIImageView!
    @IBOutlet weak var fcastMaxTempLabel1: UILabel!
    @IBOutlet weak var fcastMinTempLabel1: UILabel!
    
    @IBOutlet weak var fcastDateLabel2: UILabel!
    @IBOutlet weak var fcastIconView2: UIImageView!
    @IBOutlet weak var fcastMaxTempLabel2: UILabel!
    @IBOutlet weak var fcastMinTempLabel2: UILabel!
    
    @IBOutlet weak var fcastDateLabel3: UILabel!
    @IBOutlet weak var fcastIconView3: UIImageView!
    @IBOutlet weak var fcastMaxTempLabel3: UILabel!
    @IBOutlet weak var fcastMinTempLabel3: UILabel!
    
    @IBOutlet weak var fcastDateLabel4: UILabel!
    @IBOutlet weak var fcastIconView4: UIImageView!
    @IBOutlet weak var fcastMaxTempLabel4: UILabel!
    @IBOutlet weak var fcastMinTempLabel4: UILabel!
    
    @IBOutlet weak var fcastDateLabel5: UILabel!
    @IBOutlet weak var fcastIconView5: UIImageView!
    @IBOutlet weak var fcastMaxTempLabel5: UILabel!
    @IBOutlet weak var fcastMinTempLabel5: UILabel!
    
    @IBOutlet weak var fcastDateLabel6: UILabel!
    @IBOutlet weak var fcastIconView6: UIImageView!
    @IBOutlet weak var fcastMaxTempLabel6: UILabel!
    @IBOutlet weak var fcastMinTempLabel6: UILabel!
    
    @IBOutlet weak var fcastDetailsButton: UIButton!
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var userListsTableView: UITableView!
    @IBOutlet weak var lookupFieldFavoritesButton: UIButton!
    
    var locationManager: CLLocationManager!
    
    var placeString : String?
    var placeName : String?
    var placeURLs : (currCondURL: String?, fcastURL: String?)
    var placeLat : String?
    var placeLon : String?
    var placeID : Int?
    var placePin = MKPointAnnotation()
    var cityLocation : CLLocation?
    var isCurrentLocation = false
    var addOn = ""
    
    struct weather : Codable
    {
        var id : Int
        var main : String
        var description : String
        var icon : String
    }
    
    struct sys : Codable {
        var country : String?
        var sunrise : Int
        var sunset : Int
    }
    
    struct WeatherData : Codable
    {
        var id : Int
        var name : String
        var coord: [String : Double]
        var weather : [weather]
        var wind: [String : Double]
        var main: [String : Double]
        var sys : sys
    }
    
    var currentWeatherData: WeatherData?
    
    struct ErrorResponse : Codable {
        var cod : String
        var message : String
    }
    
    struct CityData : Codable {
        var countryName : String
        var gmtOffset : Int
        var abbreviation : String
    }
    
    var cityData: CityData?
    
    struct ForecastData : Codable
    {
        var list: [liststuff]
    }
    
    var forecastData : ForecastData?
    
    struct city : Codable
    {
        var id: Int
        var name: String
        var coord: [String : Double]
        var country: String
    }
    
    struct liststuff : Codable
    {
        var dt: Int
        var main: [String : Double]
        var weather: [weather]
        var wind: [String : Double]
        var dt_txt: String
    }
    
    struct ShortForecastData {
        var date = Date()
        var id = 0
        var iconId = ""
        var maxTemp = 0
        var minTemp = 0
    }
    
    typealias ForecastTableStuff = (time: String, cond: String, temp: Int, wind: String)
    
    var fcastTableData = [String : [ForecastTableStuff]]()
    var shortFcastTableData = [String : [ForecastTableStuff]]()
    var fcastTabledateKeys = [String]()
    var shortFcastTabledateKeys = [String]()
    var fcastTableValues = [ForecastTableStuff]()
    
    var shortForecast = [ShortForecastData]()
    
    let sections = ["My Places", "Recent Places"]
    
//*******************************************************************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        placeLookupField.delegate = self
        makeSearchBar()
        setCurrentLocation()
        
        if UserDefaults.standard.stringArray(forKey: "favoritePlaces") == nil {
            UserDefaults.standard.set([String](), forKey: "favoritePlaces")
        }
        
        if UserDefaults.standard.stringArray(forKey: "recentPlaces") == nil {
            UserDefaults.standard.set([String](), forKey: "recentPlaces")
        }
        
        getWeather(self.placeURLs.currCondURL!, completion: {weatherStuff in
            
            self.currentWeatherData = weatherStuff
            if self.currentWeatherData != nil {
            self.placeName = self.currentWeatherData!.name
            self.placeID = self.currentWeatherData!.id
            
            self.getCityData(cityLocation: self.cityLocation!, completion: {cityData in
                self.cityData = cityData!
                
            self.currentConditionsOutput()
            self.getForecast(self.placeURLs.fcastURL!)
            
            DispatchQueue.main.async {
                let placeString = self.placeLookupField.text
                self.lookupFieldFavoritesButton.setImage(UIImage(named: self.favoritesButtonImage(placeString!)), for: UIControl.State.normal)
                if self.isCurrentLocation {self.addOn = "Current: "}
                self.placeLookupField.text = self.addOn + self.placeLookupField.text!
                }
                
            }) }
            })
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return sections[section]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sections.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let recentList = UserDefaults.standard.array(forKey: "recentPlaces") as! [String]
        let favoritesList = UserDefaults.standard.array(forKey: "favoritePlaces") as! [String]
        
        switch section {
        case 0:
            return favoritesList.count
        case 1:
            return recentList.count
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let recentList = UserDefaults.standard.array(forKey: "recentPlaces") as! [String]
        let favoritesList = UserDefaults.standard.array(forKey: "favoritePlaces") as! [String]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "userListCell", for: indexPath) as! userListCell
        
        cell.textLabel!.font.withSize(12)
        
        switch indexPath.section {
        case 0:
            cell.textLabel?.text = favoritesList[indexPath.row]
        case 1:
            cell.textLabel?.text = recentList[indexPath.row]
        default:
            cell.textLabel?.text = ""
        }
        
        cell.favoritesButton.setImage(UIImage(named: favoritesButtonImage(cell.textLabel!.text!)), for: UIControl.State.normal)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let indexPath = tableView.indexPathForSelectedRow
        let currentCell = tableView.cellForRow(at: indexPath!)
        placeLookupField.text = currentCell?.textLabel?.text
        tableView.isHidden = true
        doSearch()
    }

    
    
    @IBAction func currLocButtonPressed(_ sender: Any) {
        
        setCurrentLocation()
        
        getWeather(self.placeURLs.currCondURL!, completion: {weatherStuff in
            
        self.currentWeatherData = weatherStuff
        if self.currentWeatherData != nil {
        self.placeName = self.currentWeatherData!.name
        self.placeID = self.currentWeatherData!.id
        
        self.getCityData(cityLocation: self.cityLocation!, completion: {cityData in
            self.cityData = cityData!
            
        self.currentConditionsOutput()
            
        self.getForecast(self.placeURLs.fcastURL!)
            
        DispatchQueue.main.async {
            self.placeLookupField.text = "Current: " + self.placeLookupField.text!
        
            let placeString = self.placeName! + ", " + self.currentWeatherData!.sys.country!
            
            let recentList = UserDefaults.standard.array(forKey: "recentPlaces") as! [String]
            let favoritesList = UserDefaults.standard.array(forKey: "favoritePlaces") as! [String]
            
            if !recentList.contains(placeString) && !favoritesList.contains(placeString) {
            self.updateUserList(placeString, "recentPlaces")
            }
            self.view.endEditing(true)
            self.userListsTableView.isHidden = true
        }
            }) }
        })
    }
    
    
    private func getIndexPath(of element: Any) -> IndexPath?
    {
        if let view =  element as? UIView
        {
            let pos = view.convert(CGPoint(x: 0,y :0), to: self.userListsTableView)
            return userListsTableView.indexPathForRow(at: pos)
        }
        return nil
    }
    
    
    @IBAction func favoritesButtonPressed(_ sender: AnyObject) {
        
        if let indexPath = getIndexPath(of: sender)
        {
            let currentCell = userListsTableView.cellForRow(at: indexPath) as! userListCell
            let currentCellText = currentCell.textLabel?.text
            
            favoritesButtonDoJob(currentCellText!, currentCell.favoritesButton)

        }
        userListsTableView.reloadData()
    }
    
    @IBAction func addFavoritesFromLookup(_ sender: Any) {
        
        if placeLookupField.text != nil && placeLookupField.text != "" {
            var placeString = placeLookupField.text!
            if placeString.hasPrefix("Current: ") {
                placeString = String(placeString.dropFirst("Current: ".count))
            }
            
            lookupFieldFavoritesButton.setImage(UIImage(named: favoritesButtonImage(placeString)), for: UIControl.State.normal)
            favoritesButtonDoJob(placeString, lookupFieldFavoritesButton)
        } else {
            lookupFieldFavoritesButton.setImage(UIImage(named: "star.png"), for: UIControl.State.normal)
        }
    }
    
    func favoritesButtonDoJob(_ placeString: String, _ thebutton: UIButton) {
        
        var recentList = UserDefaults.standard.array(forKey: "recentPlaces") as! [String]
        var favoritesList = UserDefaults.standard.array(forKey: "favoritePlaces") as! [String]
        
        if recentList.contains(placeString) {

            updateUserList(placeString, "favoritePlaces")
            thebutton.setImage(UIImage(named: "ystar.png"), for: UIControl.State.normal)
            recentList = recentList.filter { $0 != placeString }
            UserDefaults.standard.set(recentList, forKey: "recentPlaces")
            userListsTableView.reloadData()
            return
        }
            
        if favoritesList.contains(placeString) {
            
            favoritesList = favoritesList.filter { $0 != placeString }
            UserDefaults.standard.set(favoritesList, forKey: "favoritePlaces")
            
            if thebutton == lookupFieldFavoritesButton {
            thebutton.setImage(UIImage(named: self.favoritesButtonImage(placeString)), for: UIControl.State.normal)
            updateUserList(placeString, "recentPlaces")
            }
            userListsTableView.reloadData()
            
            if recentList.contains(placeString) {
                recentList = recentList.filter { $0 != placeString }
                UserDefaults.standard.set(recentList, forKey: "recentPlaces")
                userListsTableView.reloadData()
            }
            userListsTableView.reloadData()
            return
        }
    }
    
    
    func favoritesButtonImage(_ cellText: String) -> String {

        let favoritesList = UserDefaults.standard.array(forKey: "favoritePlaces") as! [String]
        
        if favoritesList.contains(cellText) {
        return "ystar.png"
        } else {
            return "star.png"
        }
    }
    
    
    @IBAction func tapGesture(_ sender: UITapGestureRecognizer) {
        if sender.state == .ended {

            placeLookupField.text = ""
            
            let tappedPoint = sender.location(in: mapView)
            let tappedCoordinate: CLLocationCoordinate2D = mapView.convert(tappedPoint, toCoordinateFrom: mapView)
            let tappedLocation = CLLocation(latitude: tappedCoordinate.latitude, longitude: tappedCoordinate.longitude)
            
            self.centerMapOnLocation(location: tappedLocation, regionRadius: 50000)
            self.placePin.coordinate = tappedLocation.coordinate
            self.mapView.addAnnotation(self.placePin)
            
            placeURLs.currCondURL = placeURLMaker(String(tappedCoordinate.latitude)+","+String(tappedCoordinate.longitude)).0
            placeURLs.fcastURL = placeURLMaker(String(tappedCoordinate.latitude)+","+String(tappedCoordinate.longitude)).1
            print(placeURLs.currCondURL!)
            
            getWeather(placeURLs.currCondURL!, completion: {(weatherstuff) in
                
                let recentList = UserDefaults.standard.array(forKey: "recentPlaces") as! [String]
                let favoritesList = UserDefaults.standard.array(forKey: "favoritePlaces") as! [String]
                
                self.currentWeatherData = weatherstuff
            if self.currentWeatherData != nil {
                self.placeName = self.currentWeatherData!.name
                self.placeID = self.currentWeatherData!.id
                self.cityLocation = CLLocation(latitude: tappedCoordinate.latitude, longitude: tappedCoordinate.longitude)
                
                self.getCityData(cityLocation: self.cityLocation!, completion: {cityData in
                    self.cityData = cityData!
                    self.currentConditionsOutput()
                    
                    self.getForecast(self.placeURLs.fcastURL!)
                    
                    var placeString = self.placeName!
                    if self.currentWeatherData!.sys.country != nil {
                        placeString = placeString + ", " + self.currentWeatherData!.sys.country!
                    }
                    
                    if !recentList.contains(placeString) && !favoritesList.contains(placeString) {
                        self.updateUserList(placeString, "recentPlaces")
                        
                    DispatchQueue.main.async {
                        self.view.endEditing(true)
                        }
                    }
                    
                }) }
            })
        }
    }
    
    
    @IBAction func searchButtonPressed(_ sender: Any) {
        
        userListsTableView.isHidden = true
        doSearch()
        print(placeString!, favoritesButtonImage(placeString!))
        lookupFieldFavoritesButton.setImage(UIImage(named: favoritesButtonImage(placeString!)), for: UIControl.State.normal)
    }
    
    func doSearch()
    {
        self.view.endEditing(true)
        placeString = placeLookupField.text
        
        let recentList = UserDefaults.standard.array(forKey: "recentPlaces") as! [String]
        let favoritesList = UserDefaults.standard.array(forKey: "favoritePlaces") as! [String]

        if (placeString != nil && placeString != "") {
            
            placeURLs.currCondURL = placeURLMaker(placeString!).0
            print(placeURLs.currCondURL!)
            placeURLs.fcastURL = placeURLMaker(placeString!).1
            
            getWeather(placeURLs.currCondURL!, completion: {(weatherStuff) in
                
                self.currentWeatherData = weatherStuff
            if self.currentWeatherData != nil {
                self.placeName = self.currentWeatherData!.name
                self.placeID = self.currentWeatherData!.id
                self.cityLocation = CLLocation(latitude: weatherStuff!.coord["lat"]!, longitude: weatherStuff!.coord["lon"]!)
                
                self.getCityData(cityLocation: self.cityLocation!, completion: {cityData in
                    self.cityData = cityData!
                    self.currentConditionsOutput()
                    
                    self.getForecast(self.placeURLs.fcastURL!)
                    
                    if self.currentWeatherData!.sys.country != nil {
                        self.placeString = self.placeName! + ", " + self.currentWeatherData!.sys.country! }
                    
                    if !recentList.contains(self.placeString!) && !favoritesList.contains(self.placeString!) {
                        self.updateUserList(self.placeString!, "recentPlaces")
                    }
                    print("Resent places:")
                    print(UserDefaults.standard.array(forKey: "recentPlaces") as! [String])
                DispatchQueue.main.async {
                    
                    if self.placeString!.hasPrefix("Current: ") {
                        self.placeString = String(self.placeString!.dropFirst("Current: ".count))
                    }
                    self.lookupFieldFavoritesButton.setImage(UIImage(named: self.favoritesButtonImage(self.placeString!)), for: UIControl.State.normal)
                    }
                }) }
            })
        }
      }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        userListsTableView.reloadData()
        userListsTableView.isHidden = false
           return true
    }

    
    
    func updateUserList(_ place: String, _ listname: String) {
        
       let ns = UserDefaults.standard
       var list = ns.array(forKey: listname) as! [String]
        
        if !list.contains(place) {
            list.insert(place, at: 0)
            }
        
        while list.count > 5 {
            list.removeLast()
        }
        
        ns.removeObject(forKey: listname)
        ns.set(list, forKey: listname)
        ns.synchronize()
    }
    
    
    func getCityData(cityLocation: CLLocation, completion: @escaping (_ cityData: CityData?) -> Void) {
 
        let cityLat = cityLocation.coordinate.latitude
        let cityLon = cityLocation.coordinate.longitude
        let dataURL = "https://api.timezonedb.com/v2.1/get-time-zone?key=NFZXK8HGLNO6&format=json&by=position&lat=\(cityLat)&lng=\(cityLon)"
        print(dataURL)
        getData(fromURL: dataURL, completion: {(result, response) in
            
            let decoder = JSONDecoder()
            if(response.statusCode != 200) {
                    print("Bad response")
                } else if response.statusCode != 400 {
                do {
                    let cityData = try decoder.decode(CityData.self, from: result)
                    completion(cityData)
                }
                catch {print("Bad JSON")
                completion(nil)
                    }
                }
        })
    }
    
    
    func currentConditionsOutput() {
        
    DispatchQueue.main.async {
        
        let firstData = self.currentWeatherData!.weather.first!
        
        var windDirection = ""
        
        if self.currentWeatherData!.wind["deg"] != nil {
            
            windDirection = self.windTrim(windDeg: self.currentWeatherData!.wind["deg"]!)
        }
        
        let tempOut = String(Int(round(self.currentWeatherData!.main["temp"]!)))+"\u{00B0} C"
        let windOut = "Wind: "+String(Int(round(self.currentWeatherData!.wind["speed"]!)))+" m/s "+windDirection
            
        self.showLocationMap(self.cityLocation!)
        
        if self.currentWeatherData!.sys.country != nil {
            self.placeLookupField.text = self.currentWeatherData!.name + ", " + self.currentWeatherData!.sys.country!
        } else {
            self.placeLookupField.text = self.currentWeatherData!.name
        }
        
        self.currCondTitle.textColor = UIColor.white
        let iconUrlString = "https://openweathermap.org/img/w/"+firstData.icon+".png"
            
        var countryNameOut = ""
        if self.cityData!.countryName != "" {
            countryNameOut = ", " + self.cityData!.countryName
        }
        self.fillCurrentConditions("Current conditions for", self.currentWeatherData!.name + countryNameOut, tempOut, firstData.description.prefix(1).capitalized+firstData.description.dropFirst(), windOut, "Humidity: "+String(Int(round(self.currentWeatherData!.main["humidity"]!)))+"%", self.getIcon(iconUrlString))
        }
    }
    
    
    func getWeather(_ placeURL: String, completion: @escaping (_ weatherData: WeatherData?) -> Void) {
        
        getData(fromURL: placeURL, completion: {(result, response) in
            
            let decoder = JSONDecoder()
            
            if(response.statusCode != 200)
   //...meaning the place hasn't been found
                
            {
                do {
                    DispatchQueue.main.async {
                        self.currCondTitle.font.withSize(12)
                        self.currCondTitle.textColor = UIColor.red
                        self.fillCurrentConditions("City not found. Try again or tap a location.", "", "", "", "", "", nil)
                        self.shortForecastUICleanup()
                        completion(nil)
                    }
                }

            } else if (response.statusCode != 404) {
    //...the place has been found and has a name
                
                do {
                    var weatherStuff = try decoder.decode(WeatherData.self, from: result)
                    if (weatherStuff.name) != "" {
                        completion(weatherStuff)
                    } else
   //...we can get weather by coordinates but the place has no name
                        
                        {weatherStuff.name = String(weatherStuff.coord["lat"]!) + ", " + String(weatherStuff.coord["lon"]!)
                            completion(weatherStuff)
                    }
                } catch {
                    DispatchQueue.main.async {
                        self.currCondTitle.font.withSize(12)
                        self.currCondTitle.textColor = UIColor.red
                        self.fillCurrentConditions("No consistent data for this area", "", "", "", "", "", nil)
                    }
                    print("Failed to decode JSON")
                    self.shortForecastUICleanup()
                    completion(nil)
                }
            }
            else {
                DispatchQueue.main.async {
                    self.currCondTitle.font.withSize(12)
                    self.currCondTitle.textColor = UIColor.red
                    self.fillCurrentConditions("City not found. Try again or tap a location.", "", "", "", "", "", nil)
                    self.shortForecastUICleanup()
                    completion(nil)
                }
            }
        })
    }

    
    func getData(fromURL: String, completion: @escaping (_ result: Data, _ response: HTTPURLResponse) -> Void) {
        let request = NSMutableURLRequest(url: URL(string: fromURL)!)
        let session = URLSession.shared
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if(error != nil)
            {
                print("ERROR CONNECT")
                return
            }
            completion(data!, response as! HTTPURLResponse)
        })
        task.resume()
    }
    
    
    func windTrim(windDeg : Double) -> String {
       var windDirection : String
        if (self.currentWeatherData!.wind["speed"] == nil) {
            self.currentWeatherData!.wind["speed"] = 0
        }
            switch Int(round(windDeg)) {
            case 0..<10: windDirection = "N"
            case 10..<80: windDirection = "NE"
            case 80..<100: windDirection = "E"
            case 100..<170: windDirection = "SE"
            case 170..<190: windDirection = "S"
            case 190..<260: windDirection = "SW"
            case 260..<290: windDirection = "W"
            case 290..<350: windDirection = "NW"
            case 350..<361: windDirection = "N"
            default: windDirection = ""
            }
        
        return windDirection
    }
    
    
    func isCoord(_ testStr: String) -> Bool {
        let coordRegEx = "^[-+]?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?),\\s*[-+]?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?)$"
        
        let coordTest = NSPredicate(format:"SELF MATCHES %@", coordRegEx)
        return coordTest.evaluate(with: testStr)
    }
    
    func isId(_ testStr: String) -> Bool {
        let regEx = "\\d*"
        let coordTest = NSPredicate(format:"SELF MATCHES %@", regEx)
        return coordTest.evaluate(with: testStr)
    }
    
    
    func placeURLMaker(_ placeString : String) -> (String, String) {
        
        if (isCoord(placeString)) {
            
            let coords = placeString.split(separator: ",")
            placeLat = coords[0].trimmingCharacters(in: .whitespaces)
            placeLon = coords[1].trimmingCharacters(in: .whitespaces)
            
            placeURLs.currCondURL = "https://api.openweathermap.org/data/2.5/weather?lat=\(placeLat!)&lon=\(placeLon!)&APPID=352322041669d059df19c0496ae7c6b6&units=metric"
            
            placeURLs.fcastURL = "https://api.openweathermap.org/data/2.5/forecast?lat=\(placeLat!)&lon=\(placeLon!)&APPID=352322041669d059df19c0496ae7c6b6&units=metric"
            
        } else if (isId(placeString)) {
            placeURLs.currCondURL = "https://api.openweathermap.org/data/2.5/weather?id=\(placeString)&APPID=352322041669d059df19c0496ae7c6b6&units=metric"
            
            placeURLs.fcastURL = "https://api.openweathermap.org/data/2.5/forecast?id=\(placeString)&APPID=352322041669d059df19c0496ae7c6b6&units=metric"
        }
          else {
            placeURLs.currCondURL = "https://api.openweathermap.org/data/2.5/weather?q=\(placeString.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)&APPID=352322041669d059df19c0496ae7c6b6&units=metric"
            
            placeURLs.fcastURL = "https://api.openweathermap.org/data/2.5/forecast?q=\(placeString.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)&APPID=352322041669d059df19c0496ae7c6b6&units=metric"
        }
        return (placeURLs.currCondURL!, placeURLs.fcastURL!)
    }

    
    func centerMapOnLocation(location: CLLocation, regionRadius: CLLocationDistance) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    
    func shortForecastConsoleOutput() {
        
        print("************************")

        print("Short forecast array has \(self.shortForecast.count) items:")
        
        for i in 0..<self.shortForecast.count {
            print(self.shortForecast[i].date)
            print("tmax =", self.shortForecast[i].maxTemp)
            print("tmin =", self.shortForecast[i].minTemp)
            print("Icon:", "https://openweathermap.org/img/w/\(self.shortForecast[i].iconId).png")
        }
    }
    
    
    func fcastTableDataMaker() {
        
        fcastTableData.removeAll()
        fcastTableValues.removeAll()
        fcastTabledateKeys.removeAll()
        
        let df = DateFormatter()
        df.dateFormat = "EEEE, MMMM dd"
        df.timeZone = TimeZone(secondsFromGMT: self.cityData!.gmtOffset)
        
        let df1 = DateFormatter()
        df1.dateFormat = "HH:mm"
        df1.timeZone = TimeZone(secondsFromGMT: self.cityData!.gmtOffset)
 
        var sortingChar = 0
        
        fcastTabledateKeys.append(String(sortingChar)+df.string(from: Date(timeIntervalSince1970: TimeInterval(forecastData!.list[0].dt))))
        shortFcastTabledateKeys.append(df.string(from: Date(timeIntervalSince1970: TimeInterval(forecastData!.list[0].dt))))
        
        var time = df1.string(from: Date(timeIntervalSince1970: TimeInterval(forecastData!.list[0].dt)))
        var cond = forecastData!.list[0].weather.first!.icon
        var temp = Int(round(forecastData!.list[0].main["temp"]!))
        
        var wind = ""
        if forecastData!.list[0].wind["deg"] != nil {
        wind = String(Int(round(forecastData!.list[0].wind["speed"]!)))+" m/s " + self.windTrim(windDeg: forecastData!.list[0].wind["deg"]!)
        } else {
            wind = String(Int(round(forecastData!.list[0].wind["speed"]!)))+" m/s "
        }
        
        fcastTableValues.append((time, cond, temp, wind))
        fcastTableData[fcastTabledateKeys[0]] = fcastTableValues
        shortFcastTableData[shortFcastTabledateKeys[0]] = fcastTableValues
        
        for i in 1..<forecastData!.list.count {
            
            time = df1.string(from: Date(timeIntervalSince1970: TimeInterval(forecastData!.list[i].dt)))
            cond = forecastData!.list[i].weather.first!.icon
            temp = Int(round(forecastData!.list[i].main["temp"]!))
            if forecastData!.list[i].wind["deg"] != nil {
            wind = String(Int(round(forecastData!.list[i].wind["speed"]!)))+" m/s " + self.windTrim(windDeg: forecastData!.list[i].wind["deg"]!)
            }
            else {
                wind = String(Int(round(forecastData!.list[i].wind["speed"]!)))+" m/s"
            }
            let values = (time, cond, temp, wind)
            
            let date2 = df.string(from: Date(timeIntervalSince1970: TimeInterval(forecastData!.list[i].dt)))
            let date1 = df.string(from: Date(timeIntervalSince1970: TimeInterval(forecastData!.list[i-1].dt)))
            
            if date1 == date2 {
                
                fcastTableValues = fcastTableData[String(sortingChar)+date1]!
                fcastTableValues.append(values)
                fcastTableData[String(sortingChar)+date1]! = fcastTableValues
                shortFcastTableData[date1]! = fcastTableValues

            } else
                 {
                    sortingChar = sortingChar + 1
                    fcastTableValues.removeAll()
                    fcastTableValues.append(values)
                    fcastTableData[String(sortingChar)+date2] = fcastTableValues
                    shortFcastTableData[date2] = fcastTableValues
                 }
             }
        }
    
    
    func shortForecastUICleanup() {
        
       let fcastIconViews = [self.fcastIconView1, self.fcastIconView2, self.fcastIconView3, self.fcastIconView4, self.fcastIconView5, self.fcastIconView6]
        
        let fcastDateLabels = [self.fcastDateLabel1, self.fcastDateLabel2, self.fcastDateLabel3, self.fcastDateLabel4, self.fcastDateLabel5, self.fcastDateLabel6]
        
        let fcastMaxTempLabels = [self.fcastMaxTempLabel1, self.fcastMaxTempLabel2, self.fcastMaxTempLabel3, self.fcastMaxTempLabel4, self.fcastMaxTempLabel5, self.fcastMaxTempLabel6]
        
        let fcastMinTempLabels = [self.fcastMinTempLabel1, self.fcastMinTempLabel2, self.fcastMinTempLabel3, self.fcastMinTempLabel4, self.fcastMinTempLabel5, self.fcastMinTempLabel6]
        
        for view in fcastIconViews {
            view!.image = nil
        }
        
        for label in fcastDateLabels {
            label!.text = "--"
        }
        
        for label in fcastMaxTempLabels {
            label!.text = "--"
        }
        
        for label in fcastMinTempLabels {
            label!.text = "--"
        }
        self.fcastDetailsButton.setTitle("", for: .normal)
    }
    
    func shortForecastUIOutput() {
        
        if self.shortForecast.count == 6 {
            
            let fcastIconViews = [self.fcastIconView1, self.fcastIconView2, self.fcastIconView3, self.fcastIconView4, self.fcastIconView5, self.fcastIconView6]
            setShortFcastIcons(fcastIconViews as! [UIImageView])
            
            let fcastDateLabels = [self.fcastDateLabel1, self.fcastDateLabel2, self.fcastDateLabel3, self.fcastDateLabel4, self.fcastDateLabel5, self.fcastDateLabel6]
            let fcastMaxTempLabels = [self.fcastMaxTempLabel1, self.fcastMaxTempLabel2, self.fcastMaxTempLabel3, self.fcastMaxTempLabel4, self.fcastMaxTempLabel5, self.fcastMaxTempLabel6]
            let fcastMinTempLabels = [self.fcastMinTempLabel1, self.fcastMinTempLabel2, self.fcastMinTempLabel3, self.fcastMinTempLabel4, self.fcastMinTempLabel5, self.fcastMinTempLabel6]
            setShortFcastLabels(fcastDateLabels as! [UILabel], fcastMaxTempLabels as! [UILabel], fcastMinTempLabels as! [UILabel])

        }
        else {
            let fcastDateLabels = [self.fcastDateLabel1, self.fcastDateLabel2, self.fcastDateLabel3, self.fcastDateLabel4, self.fcastDateLabel5]
            let fcastIconViews = [self.fcastIconView1, self.fcastIconView2, self.fcastIconView3, self.fcastIconView4, self.fcastIconView5]
            let fcastMaxTempLabels = [self.fcastMaxTempLabel1, self.fcastMaxTempLabel2, self.fcastMaxTempLabel3, self.fcastMaxTempLabel4, self.fcastMaxTempLabel5]
            let fcastMinTempLabels = [self.fcastMinTempLabel1, self.fcastMinTempLabel2, self.fcastMinTempLabel3, self.fcastMinTempLabel4, self.fcastMinTempLabel5]
            setShortFcastIcons(fcastIconViews as! [UIImageView])
            setShortFcastLabels(fcastDateLabels as! [UILabel], fcastMaxTempLabels as! [UILabel], fcastMinTempLabels as! [UILabel])
            
            DispatchQueue.main.async {
                self.fcastIconView6!.image = nil
                self.fcastDateLabel6.text = "--"
                self.fcastMaxTempLabel6.text = "--"
                self.fcastMinTempLabel6.text = "--"
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let vc = segue.destination as? ForecastViewController
        {
            vc.placeName = placeName!
            vc.countryName = cityData!.countryName
            vc.forecastData = forecastData!
            vc.fcastTableData = fcastTableData
        }
    }

    
    func getForecast(_ fcastURL: String) {
        
        shortForecast.removeAll()
        print(fcastURL)
        
        getData(fromURL: fcastURL, completion: {(result, response) in
            
            let decoder = JSONDecoder()
            
            if(response.statusCode != 200) {
                do {
                    let resp = try decoder.decode(ErrorResponse.self, from: result)
                    print(resp.message)
                } catch {
                    print("Unknown error")
                }
                
            }  else if (response.statusCode != 404) {
                
                do {
                    let fcaststuff = try decoder.decode(ForecastData.self, from: result)
  
                    self.forecastData = fcaststuff
                    self.fcastTableDataMaker()
                    self.getPrevailingCondition()
                    self.shortForecastUIOutput()
                    
                    DispatchQueue.main.async {
                        self.fcastDetailsButton.setTitle("details \u{27F6}", for: .normal)
                    }
                    
                } catch {
                    print("Failed to decode JSON")
                }
            }
        })
    }
    
    
    func getPrevailingCondition() {
        
        let df = DateFormatter()
        df.dateFormat = "MMMM d, yyyy"
        
        let dfy = DateFormatter()
        dfy.dateFormat = "YYYY"
        
        for (date) in fcastTableData.keys.sorted() {
            
            var sf = ShortForecastData()
            
            var dateString = date.dropFirst()
            let pnt = dateString.firstIndex(of: " ")!
            dateString = dateString[pnt...].dropFirst()
            
            dateString = dateString + ", " + dfy.string(from: Date())

            sf.date = df.date(from: String(dateString))!
            
            var tmax = fcastTableData[date]![0].temp
            var tmin = fcastTableData[date]![0].temp
            var refcond = (fcastTableData[date]![0].cond, 1)
            var condCount = 0
            
            for j in 1..<fcastTableData[date]!.count {
                if fcastTableData[date]![j].temp > tmax {tmax = fcastTableData[date]![j].temp}
                if fcastTableData[date]![j].temp < tmin {tmin = fcastTableData[date]![j].temp}
                if fcastTableData[date]![j].cond.dropLast() == refcond.0.dropLast() {
                    refcond.1 += 1
                } else {
                    condCount += 1
                    if condCount > refcond.1 {
                        refcond = (fcastTableData[date]![j].cond, condCount)
                    }
                }
            }
            sf.iconId = refcond.0.dropLast()+"d"
            sf.maxTemp = tmax
            sf.minTemp = tmin
            self.shortForecast.append(sf)
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(textField == placeLookupField)
        {
            userListsTableView.isHidden = true
            doSearch()
        }
        
        return true
    }
    
    func makeSearchBar() {
        placeSearchButton.layer.cornerRadius = 4
        placeLookupField.layer.cornerRadius = 15
        placeLookupField.clearButtonMode = .always
    }
    
    
    func setCurrentLocation() {
        self.locationManager = CLLocationManager()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.showsUserLocation = true
        
        self.cityLocation = locationManager.location
        let initAreaRadius: CLLocationDistance = 50000
        if self.cityLocation == nil {self.cityLocation = CLLocation(latitude: 48.8566, longitude: 2.3515)
        } else {isCurrentLocation = true}
        self.centerMapOnLocation(location: self.cityLocation!, regionRadius: initAreaRadius)
        
        placeURLs.currCondURL = placeURLMaker(String(self.cityLocation!.coordinate.latitude)+","+String(self.cityLocation!.coordinate.longitude)).0
        placeURLs.fcastURL = placeURLMaker(String(self.cityLocation!.coordinate.latitude)+","+String(self.cityLocation!.coordinate.longitude)).1
    }
    
    
    func showLocationMap(_ cityLocation: CLLocation) {
        self.centerMapOnLocation(location: cityLocation, regionRadius: 50000)
        self.placePin.coordinate = cityLocation.coordinate
        self.mapView.addAnnotation(self.placePin)
    }
    
    func getIcon(_ iconURLString: String) -> UIImage {
        let iconUrl : URL = URL(string: iconURLString)!
        let iconData:NSData? = NSData(contentsOf: iconUrl)
        
        var icon: UIImage = UIImage(named: "noicon.png")!
        
        if iconData != nil {
            icon = UIImage(data: iconData! as Data)!
        }
            return icon
        }
    
    
    func fillCurrentConditions(_ currCondTitleText: String, _ currCondPlaceLabelText: String, _ currCondTempLabelText: String, _ currCondDescLabelText: String, _ currCondWindLabelText: String, _ currCondHumLabelText: String, _ currentWeatherIconViewImage: UIImage?) {
  
            self.currCondTitle.text = currCondTitleText
            self.currCondPlaceLabel.text = currCondPlaceLabelText
            self.currCondTempLabel.text = currCondTempLabelText
            self.currCondDescLabel.text = currCondDescLabelText
            self.currCondWindLabel.text = currCondWindLabelText
            self.currCondHumLabel.text = currCondHumLabelText
            self.currentWeatherIconView.image = currentWeatherIconViewImage
    }
    
    
    func setShortFcastIcons(_ iconViews: [UIImageView]) {
        
        for (i, iconView) in iconViews.enumerated() {
            
            let iconUrl : URL = URL(string: "https://openweathermap.org/img/w/\(self.shortForecast[i].iconId).png")!
            
            let iconData:NSData = NSData(contentsOf: iconUrl)!
            DispatchQueue.main.async {
                let icon = UIImage(data: iconData as Data)
                iconView.image = icon
            }
        }
    }
    
    func setShortFcastLabels(_ dateLabels: [UILabel], _ maxTempLabels: [UILabel], _ minTempLabels: [UILabel]) {
        
        let df = DateFormatter()
        
        for (i, dateLabel) in dateLabels.enumerated() {
                        DispatchQueue.main.async {
                            df.dateFormat = "EEE"
                            let weekday = df.string(from: self.shortForecast[i].date)
                            df.dateFormat = "MMM dd"
                            let monthday = df.string(from: self.shortForecast[i].date)
                            dateLabel.text = weekday + "\n" + monthday
                    }
        }
        
        for (i, label) in maxTempLabels.enumerated() {
                DispatchQueue.main.async {
                    label.text = "\(self.shortForecast[i].maxTemp)\u{00B0}"
                }
        }
        
        for (i, label) in minTempLabels.enumerated() {
            DispatchQueue.main.async {
                label.text = "\(self.shortForecast[i].minTemp)\u{00B0}"
            }
        }
        
    }
    
    
    @IBAction func tapBlueView(_ sender: Any) {
        
        userListsTableView.isHidden = true
        self.view.endEditing(true)
    }
}


class userListCell: UITableViewCell {
    
    @IBOutlet weak var favoritesButton: UIButton!
}


